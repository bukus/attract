import Vue from 'vue'
import Vuex from 'vuex'
import category from '@/assets/js/data/category'
import city from '@/assets/js/data/city'
import products from '@/assets/js/data/data'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    filters: undefined,
    products: undefined
  },
  mutations: {
    setFilterData (state, payload) {
      state.filters = payload
    },
    setProducts (state, payload) {
      state.products = payload
    }
  },
  getters: {
    returnFilterData (state) {
      return state.filters
    },
    returnProducts (state) {
      return state.products
    }
  },
  actions: {
    getAllFilterData (context) {
      let filters = {}
      filters.city = city
      filters.category = category
      context.commit('setFilterData', filters)
    },
    getProductsActions (context) {
      context.commit('setProducts', products)
    }
  },
  modules: {
  }
})
