<template>
  <div class="container">
    <div class="header"></div>
    <div class="main-content">
      <div class="row">
        <div class="col-lg-3" v-if="getFilters">
          <div class="filter-block">
            <h4 class="filter-caption text-left font-weight-bold">City</h4>
            <select class="custom-select" v-model="filters.city">
              <option value="" selected>Select City</option>
              <option v-for="item in getFilters.city" :key="item.id" :value="item.id">{{item.name}}</option>
            </select>
          </div>
          <div class="filter-block">
            <h4 class="filter-caption text-left font-weight-bold">Categories</h4>
            <div class="form-check text-left" v-for="item in getFilters.category">
              <input class="form-check-input" type="checkbox" :value="item.id" :id="`defaultCheck${item.id}`" @click="selectCategories(item.id)" :checked="checkedCategory(item.id)">
              <label class="form-check-label" :for="`defaultCheck${item.id}`">
                {{item.name}}
              </label>
            </div>
          </div>
          <div class="filter-block">
            <h4 class="filter-caption text-left font-weight-bold">Price</h4>
            <vue-slider
              v-model="filters.price"
              :order="true"
              :max="maxPrice"
              :min="minPrice"
            ></vue-slider>
            <div class="footer-block-filter">
              <div class="font-weight-bold text-left">${{ filters.price[0] }} - ${{filters.price[1]}}</div>
              <button class="filter-btn" @click="getProducts">Filter</button>
            </div>
          </div>
        </div>
        <div class="col-lg-9">
          <div class="row">
            <card-item v-for="item in products" :item="item" :key="item.id" class="col-lg-4"></card-item>
          </div>
        </div>
      </div>
    </div>
    <div class="footer"></div>
  </div>
</template>

<script>
import VueSlider from 'vue-slider-component'
import 'vue-slider-component/theme/material.css'
import cardItem from '../components/cardProduct'

export default {
  name: 'home',
  components: {
    VueSlider,
    cardItem
  },
  data () {
   return {
     products: undefined,
     filters: {
       city: '',
       category: [],
       price: [0, 9999]
     }
   }
  },
  computed: {
    getFilters() {
      return this.$store.getters.returnFilterData
    },
    maxPrice(){
        let maxPrice = 0
        for (let i in this.$store.getters.returnProducts) {
          if (this.$store.getters.returnProducts[i].price > maxPrice) {
            maxPrice = this.$store.getters.returnProducts[i].price
          }
        }
        return maxPrice
    },
    minPrice(){
        let minPrice = this.maxPrice
        for (let i in this.$store.getters.returnProducts) {
          if (this.$store.getters.returnProducts[i].price < minPrice) {
            minPrice = this.$store.getters.returnProducts[i].price
          }
        }
        return minPrice
    }
  },
  methods: {
    checkedCategory (id) {
      return !this.filters.category.indexOf(id)
    },
    parseFilterCookie () {
      if (this.$cookie.get('filter')){
        this.filters = JSON.parse(this.$cookie.get('filter'))
      }
      this.getProducts()
    },
    getProducts () {
      let products = this.$store.getters.returnProducts
      if (products) {
        if (this.filters.city !== '') {
          products = products.filter(item => item.city === this.filters.city)
        }
        if (this.filters.category.length !== 0) {
          products = products.filter(item => {
            if (this.filters.category.indexOf(item.category) === 0) {
              return item
            }
          })
        }
        products = products.filter(x => x.price >= this.filters.price[0] && x.price <= this.filters.price[1])
      }
      this.products = products
      this.$cookie.set('filter', JSON.stringify(this.filters))
    },
    selectCategories (id) {
      if (!this.filters.category.find(x => x === id)){
        this.filters.category.push(id)
      } else {
        this.filters.category.splice(this.filters.category.indexOf(id), 1)
      }
    },
  },
  mounted () {
    this.$store.dispatch('getAllFilterData')
    this.$store.dispatch('getProductsActions')
    this.parseFilterCookie()
  },
  watch: {
    'minPrice': function () {
      this.filters.price.splice(0, 1, this.minPrice)
    },
    'maxPrice': function () {
      this.filters.price.splice(1, 1, this.maxPrice)
    },
  }
}
</script>
<style scoped lang="scss">
  .header, .footer{
    height: 190px;
    background-color: #dedfe0;
  }
  .main-content {
    margin: 50px 0 80px;
    .filter-block{
      margin-bottom: 60px;
    }
    .filter-caption {
      margin-bottom: 30px;
    }
  }
  .filter-btn{
    font-size: 16px;
    padding: 5px 10px;
    width: 100px;
    text-align: center;
    background-color: #1cb8ff;
    color: #fff;
    box-shadow: none;
    border: none;
  }
  .footer-block-filter {
    margin-top: 20px;
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: space-between;
    align-items: center;
    align-content: stretch;
  }
</style>
