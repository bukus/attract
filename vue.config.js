module.exports = {
  lintOnSave: false,
  devServer: {
    disableHostCheck: true
  },
  css: {
    sourceMap: true
  },
  productionSourceMap: false,
}
